var jwt = require('jsonwebtoken');

module.exports = {
    signToken (payload) {
       
        const token = jwt.sign(payload, sails.config.jwt.secret, {
            expiresIn: 10000000
        });
        return token;
    },
    verifyToken (token, cb) {
        return jwt.verify(token, sails.config.jwt.secret, cb);
    }
}
