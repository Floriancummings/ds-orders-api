/**
 * Food.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 module.exports = {

 	attributes: {
 		name:{
 			type:'string'
 		},
 		price:{
 			type:'integer'
 		},
 		owner:{
 			model:'user',
 			
 		},
 		ordered:{
 			type:'boolean',
 			defaultsTo:false
 		},
 		orderTime:{
 			type:'date'
 		}
 	},
 	afterCreate: function (value, cb) {

 		User.findOne({id:value.owner}).then(user=>{
 			const w=user.wallet-value.price;
 			User.update({id:value.owner},{wallet:w}).then(updatedUser=>{
 				return cb()
 			}).catch(e=>cb(e))
 		}).catch(err=>cb(err))
 	},
 	afterDestroy(values,cb){
 		User.findOne({id:values[0].owner}).then(user=>{
 			const w=user.wallet+values[0].price;
 			User.update({id:values[0].owner},{wallet:w}).then(updatedUser=>{
 			return cb()
 			}).catch(e=>cb(e))
 		}).catch(err=>cb(err))
 	}
 };

