/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create(req, res) {
		const user = {
			username: req.body.username,
			password:req.body.password,
			email: req.body.email,
			number:req.body.number,
			wallet:300
		}
		User.create(user).exec(function(err, createdUser) {
			if (err) {
				if(err.invalidAttributes && err.invalidAttributes.email && err.invalidAttributes.email[0]
				&& err.invalidAttributes.email[0].rule === 'email') return res.badRequest('invalid email address');

				if(err.invalidAttributes && err.invalidAttributes.email && err.invalidAttributes.email[0]
				&& err.invalidAttributes.email[0].rule === 'unique')  return res.badRequest('Email already used');

				if(err.invalidAttributes && err.invalidAttributes.username && err.invalidAttributes.username[0]
				&& err.invalidAttributes.username[0].rule === 'unique')  return res.badRequest('Username already Taken');
					
				return res.negotiate('unknown error');
			}
	      
					const token = Jwt.signToken({id:createdUser.id});
					return res.json({user:createdUser,token: token})
		});
	},
	readOne(req, res) {

	    User.findOne({id:req.param('id')}).exec(function foundUser(err, user) {

	      if (err) return res.negotiate(err);

	      if (!user) return res.notFound();

	      return res.json(user);
	    });
  	},
	list (req,res) {
		User.find().exec(function (err,users) {
			if(err) return res.negotiate(err);
			return res.json(users);
		})
	},
	cash(req,res){
		User.findOne({id:req.user.id}).then(user=>res.json(user.wallet))
		.catch(err=>res.notFound(err))
	},
	creditUsers(req,res){
		const amount = req.body.amount;
		if(!/^\d+$/.test(amount))
			return res.forbidden("Invalid price")
		CreditService.credit(amount,food=>{
       	if(food.length === 0){
       		User.update({},{wallet:amount}).then(users=>res.ok(`All users have been credited with #${amount}`))
       	.catch(err=>res.forbidden(err))
       	}else{
       		return res.forbidden("Some orders are still pending")
       	}
       },err=>res.forbidden("something went wrong"))
	},
	makeRequest(req,res){
		User.update({id:req.user.id},{wallet:0}).then(updated=>{
			return res.json(updated[0]);
		}).catch(err=>res.negotiate(err))
	}
	
	// update:function (req,res) {
	// 	User.update({
	// 		id: req.param('id')
	// 	},
	// 		req.allParams()
	// 	).exec(function(err, updatedUser) {
	// 		return res.json(updatedUser);
	// 	});
	// },
	// delete:function (req,res) {
	// 	User.destroy({
	// 		id: req.param('id')
	// 	}).exec(function(err, delUser) {
	// 		if(err) return res.negotiate(err)
	// 	});
	// },
	
	// updateAdmin:function(req,res) {
	// 	 User.findOne({id:req.param('id')})
	// 	 .exec(function foundUser(err, user) {

	//       if (err) return res.negotiate(err);

	//       if (!user) return res.forbidden();
	//       User.update({
	// 		id: req.param('id')
	// 		},
	// 		{admin:!user.admin}
	// 		).exec(function(err, updatedUser) {
	// 		return res.json(updatedUser[0]);
	// 		});
	      
	      
	//     	});
	// }
};


