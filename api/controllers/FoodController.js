/**
 * FoodController
 *
 * @description :: Server-side logic for managing foods
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	add(req,res){
		const food={
			name:req.body.name,
			price:req.body.price,
			owner:req.user.id
		}
		Food.create(food).then(food=>{
			User.findOne({id:req.user.id}).then(user=>res.json(user)).catch(err=>res.negotiate(err))

		})
		.catch(err=>res.negotiate(err))

	},
	delete(req,res){

		Food.destroy({id:req.param('id')}).then(delfood=>{
			User.findOne({id:req.user.id}).then(user=>res.json(user)).catch(err=>res.negotiate(err))
		})
		.catch(err=>res.negotiate(err))
	},
	listPerUser(req,res){
		Food.find({owner:req.user.id,ordered:false}).then(food=>{
			return res.json(food)

		}).catch(err=>{
			return res.negotiate(err)
		})
	},
	
	adminlist(req,res){
		Food.find({ordered:true}).populate('owner').then(food=>{
			return res.json(food)

		}).catch(err=>{
			return res.negotiate(err)
		})
	},
	makeOrder(req,res){
		const time = new Date();
		Food.update({owner:req.user.id,ordered:false},{ordered:true,orderTime:time}).then(orders=>{
			if(orders.length === 0) return res.forbidden('No food found');

			return res.ok('Orders made, waiting for admin signature')
		})
	},
	clearOrders(req,res){
		Food.destroy({owner:req.user.id}).then(deletedfood=>{
			return res.ok("Deleted All food");
		}).catch(err=>res.negotiate(err))
	}
};

