/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {
 	login(req,res) {

 		User.findOne({
 			username:req.body.username
 		})
 		.exec(function (err,user) {
 			if(err) return res.forbidden('invalid username or password');
 			if(user){

 				if(user.comparePassword(req.body.password)){
 					const token = Jwt.signToken({id:user.id});
 					return res.json({user:user,token:token})
 				}
 				else{
 					return res.forbidden('invalid username or password')
 				}
 			}
 			else{
 				return res.forbidden('invalid username or password')
 			}

 		})
 	},
 	validateToken(req,res){
 		const token = req.headers['authorization'] ? req.headers['authorization'].split(' ')[1] : '';
 		Jwt.verifyToken( token,(err, decoded) =>{
 			if (err) return res.forbidden('Invalid Token');
 			User.findOne({id:decoded.id})
 			.then(function (user) {
 				if (!user) return res.forbidden('Token Expired')

 				return res.json(user);

 			},function(err){
 				return res.forbidden('Token Expired')
 			})

 		});
 	}
 };

