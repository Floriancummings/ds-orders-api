
module.exports = function(req,res,next) {
	if(!/^\d+$/.test(req.body.price))
		return res.forbidden("Invalid price")
	if(req.user.wallet === 0)
		return res.forbidden("No cash left")
	if(req.body.price > req.user.wallet )
		return res.forbidden("Not enough cash")
	return next();
}
