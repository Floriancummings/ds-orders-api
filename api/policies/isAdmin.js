
module.exports = function(req,res,next) {
	if(req.user.role!=='admin')
		return res.forbidden("This requires Admin Authorization")

	return next();
}
