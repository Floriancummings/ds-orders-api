

module.exports = function(req,res,next) {
	const token = req.headers['authorization'] ? req.headers['authorization'].split(' ')[1] : '';
	Jwt.verifyToken( token,function(err, decoded) {
		if (err) return res.forbidden("Token Expired");
		User.findOne({id:decoded.id})
		.then(function (user) {
			if (!user) return res.forbidden("Token Expired")
				req.user = user
			return next()
			
		},function(err){
			return res.forbidden("Token Expired")
		})
		
	});
}
