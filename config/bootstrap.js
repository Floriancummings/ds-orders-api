/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {
	const users =[{
        email: 'test1@dsfood.com',
        username: 'test1',
        password: '1111',
        number:'0952521514',
        wallet:300,
        role:'admin'
        
      },{
        email: 'test2@dsfood.com',
        username: 'test2',
        password: '1111',
        number:'0952521514',
        wallet:300
        
      },{
        email: 'test3@dsfood.com',
        username: 'test3',
        password: '1111',
        number:'0125789411',
        wallet:300
        
      }]
  User.create(users).exec(function(err, createdUsers) {
    if (err) {
      return cb(err)
    } 
    
    Food.create([
      {name:'rice',price:100,owner:createdUsers[0].id},
      {name:'plantain',price:50,owner:createdUsers[1].id},
      ]).exec((err,food)=>{
            return cb();
      })
    
  });
  
};
